## 功能介绍 
旅游景区门户小程序是一个为游客朋友提供多元化服务的一站式数字文旅平台，提供“吃、住、游、购、娱”等旅游行程中的多种服务，全面支持景区介绍，景点攻略，景点预约，停车预约、景区导览等功能，游客可以在小程序中了解旅游度假区的景点信息、历史文化、各类活动等。前后端代码完整，采用腾讯提供的小程序云开发解决方案，无须服务器和域名。

- 景点预约管理：开始/截止时间/人数均可灵活设置，可以自定义客户预约填写的数据项
- 景点预约凭证：支持线下到场后校验签到/核销/二维码自助签到等多种方式
- 详尽的预约数据：支持预约名单数据导出Excel，打印

 ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

![输入图片说明](demo/%E6%97%85%E6%B8%B8%E6%99%AF%E5%8C%BA%E9%97%A8%E6%88%B7%E5%B0%8F%E7%A8%8B%E5%BA%8F%20(2).jpeg)

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信: 
 
![输入图片说明](demo/author-base.png)


## 演示 
  ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

## 安装

- 安装手册见源码包里的word文档




## 截图

![输入图片说明](demo/1%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/2%E6%99%AF%E7%82%B9.png)
![输入图片说明](demo/3%E6%94%BB%E7%95%A5.png)

![输入图片说明](demo/4%E6%99%AF%E7%82%B9%E8%AF%A6%E6%83%85.png)
![输入图片说明](demo/5%E6%99%AF%E7%82%B9%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/6%E9%A2%84%E7%BA%A6%E8%AF%A6%E6%83%85.png)

 ![输入图片说明](demo/7%E7%BE%8E%E9%A3%9F.png)
![输入图片说明](demo/8%E7%89%B9%E4%BA%A7.png)
![输入图片说明](demo/9%E5%81%9C%E8%BD%A6%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/10%E6%9C%8D%E5%8A%A1.png)

## 后台管理系统截图 
- 后台超级管理员默认账号:admin，密码123456，请登录后台后及时修改密码和创建普通管理员。
![输入图片说明](demo/11%E5%90%8E%E5%8F%B0%E7%AE%A1%E7%90%86.png)

![输入图片说明](demo/12%E5%90%8E%E5%8F%B0-%E5%86%85%E5%AE%B9%E7%AE%A1%E7%90%86.png)

![输入图片说明](demo/13%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/14%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E8%8F%9C%E5%8D%95.png)
![输入图片说明](demo/15%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%97%B6%E9%97%B4%E8%AE%BE%E7%BD%AE.png)
![输入图片说明](demo/16%E5%90%8E%E5%8F%B0-%E6%94%BB%E7%95%A5.png)
![输入图片说明](demo/17%E5%90%8E%E5%8F%B0-%E6%99%AF%E7%82%B9%E7%AE%A1%E7%90%86.png)